import { lazy } from 'react';
import { Navigate, useRoutes } from 'react-router-dom';
import { isUserLoggedIn } from 'utils/Utils';

interface Route {
  path: string;
  element: JSX.Element;
}

interface RouterProps {
  allRoutes: Route[];
}

const Login = lazy(() => import('app/modules/login'));
const Dashboard = lazy(() => import('app/modules/dashboard'));

const Router = ({ allRoutes }: RouterProps) => {
  const getHomeRoute = () => {
    const user = isUserLoggedIn();
    if (user) {
      return <Dashboard />;
    } else {
      return <Navigate replace to="/login" />;
    }
  };

  const routes = useRoutes([
    {
      path: '/',
      index: true,
      element: getHomeRoute(),
    },
    {
      path: '/login',
      element: <Login />,
    },
    {
      path: '*',
      element: getHomeRoute(),
    },
    ...allRoutes,
  ]);

  return routes;
};

export default Router;
