import { createTheme } from '@mui/material/styles';

// A custom theme for this app
const theme = createTheme({
  typography: {
    fontFamily: ['"Noto Sans"', '"Ubuntu"', '"Helvetica Neue"', 'sans-serif'].join(','),
  },
  components: {
    MuiCssBaseline: {
      styleOverrides: {
        html: {
          height: '100%',
          // scrollBehavior: 'smooth',
        },
        body: {
          WebkitFontSmoothing: 'antialiased',
          MozOsxFontSmoothing: 'grayscale',
          fontFamily: ['"Noto Sans"', '"Ubuntu"', '"Helvetica Neue"', 'sans-serif'].join(','),
          height: '100%',
          overflowY: 'scroll',
        },
        p: {
          margin: '0',
        },
      },
    },
  },
  palette: {
    primary: {
      main: '#BAA182',
    },
    secondary: {
      main: '#7B7B7B',
    },
    error: {
      main: '#BA1A1A',
    },
    text: {
      primary: '#4D4D4D',
    },
    background: {
      default: '#f8f8f8',
    },
  },
});

export default theme;
