import { Box, Container, Grid, Typography } from '@mui/material';
import monitorImg from 'assets/images/imac_dig_twins.png';
import mainLogo from 'assets/logos/Symbol.png';
import { Form, Links } from './components';

const Login = () => {
  return (
    <Container maxWidth="xl">
      <Grid container mt="71px" alignItems="center">
        <Grid item lg={5} md={6} sm={12}>
          <img src={mainLogo} alt="Logo" />
          <Typography variant="h1" mt="32px" fontSize="52px" lineHeight="64px">
            Welcome to the <br /> Product Security Platform
          </Typography>
          <Form />
        </Grid>
        <Grid item lg={7} md={6} sm={12} sx={{ display: { xs: 'none', md: 'flex' } }}>
          <Box component="img" alt="Monitor with app demo" src={monitorImg} maxWidth="100%" />
        </Grid>
      </Grid>
      <Links />
    </Container>
  );
};

export default Login;
