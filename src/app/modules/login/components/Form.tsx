import ErrorOutlineRoundedIcon from '@mui/icons-material/ErrorOutlineRounded';
import { Box, Button, InputAdornment, InputLabel, Link, TextField, Typography } from '@mui/material';
import { ErrorData, handleLogin, loginError } from 'app/redux/authentication';
import { useLoginMutation } from 'app/redux/services/auth';
import { AppDispatch } from 'app/redux/store';
import { Controller, SubmitHandler, useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';

interface IFormInput {
  email: string;
  password: string;
}

const defaultValues: IFormInput = {
  email: '',
  password: '',
};

const Form = () => {
  const dispatch = useDispatch<AppDispatch>();
  const navigate = useNavigate();
  const [login] = useLoginMutation();
  const {
    control,
    register,
    handleSubmit,
    setError,
    formState: { errors },
  } = useForm({ defaultValues });

  const onSubmit: SubmitHandler<IFormInput> = async (data) => {
    try {
      const user = await login(data).unwrap();
      dispatch(handleLogin({ userData: user }));
      navigate('/');
    } catch (error) {
      dispatch(loginError({ error: error as ErrorData }));
      setError('email', {
        type: 'manual',
      });
      setError('password', {
        type: 'custom',
        message: 'Username or password is incorrect',
      });
    }
  };

  return (
    <Box component="form" noValidate autoComplete="off" maxWidth={400} mt="36px" onSubmit={handleSubmit(onSubmit)}>
      <Box component="div" mb="28px">
        <InputLabel htmlFor="email" error={errors.email && true} sx={{ ml: '16px' }}>
          Username
        </InputLabel>
        <Controller
          name="email"
          control={control}
          render={({ field }) => (
            <TextField
              type="email"
              color="secondary"
              id="email"
              size="small"
              fullWidth
              focused
              error={errors.email && true}
              InputProps={{
                endAdornment: errors.email ? (
                  <InputAdornment position="start">
                    <ErrorOutlineRoundedIcon color="error" />
                  </InputAdornment>
                ) : null,
              }}
              {...register('email', {
                required: true,
                pattern: {
                  value: /\S+@\S+\.\S+/,
                  message: 'Please enter a valid email',
                },
              })}
              {...field}
            />
          )}
        />
        {errors.email && (
          <Typography variant="caption" color="error">
            {errors.email.message}
          </Typography>
        )}
      </Box>
      <Box component="div">
        <InputLabel htmlFor="password" error={errors.password && true} sx={{ ml: '16px' }}>
          Password
        </InputLabel>
        <Controller
          name="password"
          control={control}
          render={({ field }) => (
            <TextField
              type="password"
              color="secondary"
              id="password"
              size="small"
              fullWidth
              focused
              error={errors.password && true}
              InputProps={{
                endAdornment: errors.password ? (
                  <InputAdornment position="start">
                    <ErrorOutlineRoundedIcon color="error" />
                  </InputAdornment>
                ) : null,
              }}
              {...register('password', {
                required: true,
              })}
              {...field}
            />
          )}
        />
        {errors.password && (
          <Typography variant="caption" color="error" display="block" mb={2}>
            {errors.password.message}
          </Typography>
        )}
      </Box>
      <Link href="#" underline="none" color="inherit">
        Forgot your password?
      </Link>
      <Box component="div" mt="48px">
        <Button variant="contained" type="submit" fullWidth sx={{ textTransform: 'initial' }}>
          Log in
        </Button>
      </Box>
    </Box>
  );
};

export default Form;
