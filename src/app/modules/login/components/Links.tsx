import { Link, Stack } from '@mui/material';

const Links = () => {
  return (
    <Stack spacing={2} direction="row" mb="40px" sx={{ mt: { xs: '16px', md: '32px', lg: 0 } }}>
      <Link href="#" underline="none" color="inherit">
        Privacy policy
      </Link>
      <Link href="#" underline="none" color="inherit">
        Terms of use
      </Link>
      <Link href="#" underline="none" color="inherit">
        Contact us
      </Link>
    </Stack>
  );
};

export default Links;
