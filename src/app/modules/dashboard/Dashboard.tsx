import { Box, Card, CardContent, Typography } from '@mui/material';
import { ErrorData, handleLogout } from 'app/redux/authentication';
import { Notification, useGetUserNotificationsQuery } from 'app/redux/services/notifications';
import { AppDispatch } from 'app/redux/store';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';

export interface UserNotificationsResponce {
  data: Notification[];
  error: ErrorData;
}

const Dashboard = () => {
  const dispatch = useDispatch<AppDispatch>();
  const navigate = useNavigate();
  const { data = [], error } = useGetUserNotificationsQuery<UserNotificationsResponce>();
  const notifications = data.find((notification) => notification.id === 1);

  useEffect(() => {
    if (error?.status === 401) {
      dispatch(handleLogout());
      navigate('/login');
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [error]);

  return (
    <Box component="div" width="100vw" height="100vh" display="flex" alignItems="center" justifyContent="center">
      <Card sx={{ maxWidth: 345, textAlign: 'center' }}>
        <CardContent>
          <Typography gutterBottom variant="h5" component="div" sx={{ mt: 4 }}>
            {notifications?.description}
          </Typography>
        </CardContent>
      </Card>
    </Box>
  );
};

export default Dashboard;
