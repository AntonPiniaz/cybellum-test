import { Suspense, useState } from 'react';
import Router from 'router/Router';
import Styled from './app.styled';

function App() {
  // This is needed for future routes expansion
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [allRoutes, setAllRoutes] = useState([]);

  return (
    <Suspense fallback={null}>
      <Styled.Box>
        <Router allRoutes={allRoutes} />
      </Styled.Box>
    </Suspense>
  );
}

export default App;
