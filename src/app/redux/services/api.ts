import { createApi, fetchBaseQuery, retry } from '@reduxjs/toolkit/query/react';
import type { RootState } from '../store';

const baseQuery = fetchBaseQuery({
  baseUrl: process.env.REACT_APP_JSON_SERVER_API_URL,
  prepareHeaders: (headers, { getState }) => {
    const token = (getState() as RootState).auth.userData.accessToken;
    if (token) {
      headers.set('authorization', `Bearer ${token}`);
    }
    return headers;
  },
});

const baseQueryWithRetry = retry(baseQuery, { maxRetries: 1 });

export const api = createApi({
  baseQuery: baseQueryWithRetry,
  tagTypes: ['Auth', 'Notifications'],
  endpoints: () => ({}),
});
