import { api } from './api';

export interface Notification {
  id: number;
  title: string;
  description: string;
  created: string;
}

export const notificationsApi = api.injectEndpoints({
  endpoints: (builder) => ({
    getUserNotifications: builder.query<Notification[], void>({
      query: () => 'notifications',
    }),
  }),
});

export const { useGetUserNotificationsQuery } = notificationsApi;
