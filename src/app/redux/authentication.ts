// ** Redux Imports
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import type { RootState } from './store';

interface DefaultUserData {
  accessToken: string;
  email: string;
}

interface UserData {
  accessToken: string;
  user: {
    email: string;
    id: number;
  };
}

interface InitialState {
  userData: UserData | DefaultUserData;
  errors: ErrorData | {};
}

export interface ErrorData {
  status: number;
  data: string;
}

const initialUser = () => {
  const item = window.localStorage.getItem('userData');
  const parsedItem = item ? JSON.parse(item) : {};

  return parsedItem as DefaultUserData;
};

const initialState: InitialState = {
  userData: initialUser(),
  errors: {},
};

export const authSlice = createSlice({
  name: 'authentication',
  initialState,
  reducers: {
    handleLogin: (state, { payload: { userData } }: PayloadAction<{ userData: UserData }>) => {
      state.userData = userData;
      state.errors = {};
      localStorage.setItem('userData', JSON.stringify({ accessToken: userData.accessToken, email: userData.user.email }));
    },
    handleLogout: () => {
      localStorage.removeItem('userData');
    },
    loginError: (state, { payload: { error } }: PayloadAction<{ error: ErrorData }>) => {
      state.errors = error;
    },
  },
});

export const { handleLogin, handleLogout, loginError } = authSlice.actions;

export default authSlice.reducer;

export const selectUserData = (state: RootState) => state.auth.userData;
export const selectErrors = (state: RootState) => state.auth.errors;
