import auth from './authentication';
import { api } from './services/api';

const rootReducer = {
  [api.reducerPath]: api.reducer,
  auth,
};

export default rootReducer;
