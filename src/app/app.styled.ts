import BoxMui from '@mui/material/Box';
import CardMediaMui from '@mui/material/CardMedia';
import { styled } from '@mui/material/styles';

const Box = styled(BoxMui)(() => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
}));

const CardMedia = styled(CardMediaMui)(({ theme }) => {
  console.log(theme);
  return {
    objectFit: 'unset',
    width: 160,
    marginTop: theme.spacing(2),
    display: 'inline-block',
  };
}) as typeof CardMediaMui;

export default {
  CardMedia,
  Box,
};
